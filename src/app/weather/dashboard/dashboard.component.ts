import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'wap-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  results = [];
  weatherUrl = 'https://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric&appid=62758c3303ff3d858b6acaa8134c6edd';
  iconList = {
    '01d': 'sun',
    '02d': 'sun',
    '03d': 'cloudy',
    '04d': 'cloudy',
    '09d': 'rain',
    '10d': 'rain',
    '11d': 'storm',
    '13d': 'storm',
    '50d': 'storm',
    '01n': 'sun',
    '02n': 'sun',
    '03n': 'cloudy',
    '04n': 'cloudy',
    '09n': 'rain',
    '10n': 'rain',
    '11n': 'storm',
    '13n': 'storm',
    '50n': 'storm',
  };
  constructor(private httpClient: HttpClient, private router: Router) {
    this.httpClient.request('GET', this.weatherUrl, { responseType: 'json' })
      .subscribe((res: any) => {
        res.list.map((item: any) => {
          this.results.push({
            name: item.name,
            temp: item.main.temp,
            tempMin: item.main.temp_min,
            tempMax: item.main.temp_max,
            icon: this.iconList[item.weather[0].icon]
          });
        });
      });
  }

  navigate(name) {
    this.router.navigate([`app/${name}`]);
  }
}
