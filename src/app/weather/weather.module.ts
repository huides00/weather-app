import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherRoutingModule } from './weather-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DetailsComponent } from './details/details.component';
import { AddComponent } from './add/add.component';
import { HttpClientModule } from '@angular/common/http';
import { WeatherCardComponent } from '../ui/weather-card/weather-card.component';
import { MatRippleModule, MatIconModule } from '@angular/material';

@NgModule({
  declarations: [
    DashboardComponent,
    DetailsComponent,
    AddComponent,
    WeatherCardComponent
  ],
  imports: [
    CommonModule,
    WeatherRoutingModule,
    HttpClientModule,
    MatRippleModule,
    MatIconModule
  ]
})
export class WeatherModule { }
