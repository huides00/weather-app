import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'wap-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.scss']
})
export class WeatherCardComponent implements OnInit {
  @Input() data;
  @Input() addCard;

  @Output() onclick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  clickCard(name) {
    this.onclick.emit(name);
  }

}
